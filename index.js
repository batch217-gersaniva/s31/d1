// Require Directive --> load a particular module node.js

let http = require("http"); //HTTP - Hyper Text Transfer Protocol
// http is a node module/application
// it consist different components
let port = 3000;

http.createServer(function (request, response){
	// writeHead
	// code - 200 --> OK, No Problem, Success
	response.writeHead(200, {"Content-Type" : "text/plain"});
	response.end("Hello World!");
}).listen(port);

// A port is a virtual point where network connections start and end.
// Each port is associated with a specific process or service
// The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to it eventually communicating with our server

console.log(`Server is running at localhost: ${port}`);